const { createSlice } = require("@reduxjs/toolkit");

let initialState = {
  soLuong: 100,
};
let numberSlice = createSlice({
  name: "numberSlice",
  initialState,
  reducers: {
    tangAction: (state, action) => {
      state.soLuong = state.soLuong + action.payload;
    },
    giamAction: (state, action) => {
      state.soLuong = state.soLuong - action.payload;
    },
  },
});

// createSlice ~ function ~ return về reducer và action tương ứng
export default numberSlice.reducer;
export let { tangAction, giamAction } = numberSlice.actions;
