import logo from "./logo.svg";
import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { tangAction } from "./toolkit/numberSlice";

function App() {
  let number = useSelector((state) => state.numberSlice.soLuong);
  let dispatch = useDispatch();

  let handleTang = () => {
    dispatch(tangAction(10));
  };

  return (
    <div className="App">
      <h1>{number}</h1>
      <button className="btn btn-success" onClick={handleTang}>
        Tăng thêm 10
      </button>
    </div>
  );
}

export default App;
